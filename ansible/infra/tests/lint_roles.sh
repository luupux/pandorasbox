
#!/bin/bash
#set -x 
while  read line; 
do
   ansible-lint $line
done <  <(find "$(dirname "${BASH_SOURCE[0]}")/../roles/" -name '*.yml' )
