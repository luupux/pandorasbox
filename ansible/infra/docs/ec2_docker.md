# Deploy ec2 instance with ansible automation

<strong><h3>Prerequisite:</h3></strong>

- Udpate the followed variable in default_and_conventions.yml file 
  
   - prj_name: 'egm.eva' ({project}.{program})
   - prj_env: lch (spoc,dev,tst,int,acc,dlt,trn,lch)
   - prj_user: common01 (or the username related to the user it will use the env)
  
  N.B. below the usefull link to create the right labels for the new env
  https://webgate.ec.europa.eu/CITnet/confluence/display/OPSYS/Full+DevOps+Resources 
  
---

<strong><h3>Deploy ec2 instance with common configuration</h3></strong> 
```
     - cd infra; ansible-playbook create_common_env.yml
```
---

<br/><strong><h3>Deploy ec2 with common_env  playbook plus additional tasks related to custom project(es: sep-eva)</h3></strong> 

 - Deploy ec2 instance with using common_env playbook plus additional tasks related to sepeva
```
    - cd infra; ansible-playbook create_sepa_env.yml
```
---

<br/><strong><h3>Where are stored secrets and details after ec2 creation</h3></strong>

At the end of execution you can find in ./inventory/{prj_name/{prg_env}/{per_user}
pem file and the config_file generated in that execution

N.b. If you need to share ssh key you can use a browser and point the ec2 ip 
the credential to download and unzip are store in workstation where ansible was executed
in /tmp/{prj_name}.{prg_env}.{per_user}.pwd
the password is the same for http and zip ,  the user for http is admin
Take care of that pwd file because if you lose it you can't fetch the file or unzip its

---

<br/><strong><h3>Delete Ec2 instance</h3></strong>
The follwed command will remove an ec2 instance who match the tags prj_name,
prg_env,prg_user definded in default_and_conventions.yml

```
     - cd infra; ansible-playbook aws_remove_env.yml
```

At the end of process all inventory information releted will be deleted