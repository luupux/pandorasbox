
# <strong>Delivery Plan</strong>
## Prerequisite:
The scope of this delivery plan
is to describe the flow you need to follow step by step in order to have <br/> 
your vdi ready to work with ansible automation and afer how
to create services on AWS.<br/>
A this stage the delivery plan assume who:
  -  you have a srv4dev Account
  -  you are a Teamadmin Group in AD directory on Aws
  -  you have a TeamAdmin policy attached to your user in AWS AD account

---
<br/>

# Step 1: Configure VDI with automation tools
- ## [Ask cli secrets confluence link](https://webgate.ec.europa.eu/CITnet/confluence/display/DEVOPSDIGIT/How+to+request+an+AWS+Access+Keys+in+SRV4DEV)
- ## [Configure Ansible on VDI](./setup_ansible_on_vdi.md)
- ## [Configure shared inventory](./inventory.md)

<br/>

# Step 2: Create environments 
- ## [Create/Delete EC2 with docker configured](./ec2_docker.md)
- ## [Create/Delete RDS](./rds.md)



