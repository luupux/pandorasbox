## Work with ansible using Amazon linux VDI
 
 - Install Ansible > 2.8 
   - setup AWS_SECRETS (common Setup described above)
   - sudo yum install python2-pip python2-passlib putty zip
   - sudo pip install bcrypt
   - sudo pip install boto 
   - sudo pip install boto3==1.12

 - Install putty and friends
   - Setup Epel repo
   - sudo yum-config-manager --enable epel
   - sudo yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
   - sudo yum install putty.x86_64
