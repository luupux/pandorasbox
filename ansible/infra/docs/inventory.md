# Inventory Definition 
When creating an  environment like EC2, RDS, etc. using automation 
you will generate a bunch of information like ip,<br>
hostname,credentials, keys who need to be saved somewhere,
**these collections of information are called inventory details.**<br>
Usually, the playbook previously created stored all inventory
details in the infra/inventory folder inside to the host who executes the playbook.<br>

# How to share the inventory

## Requirements
To share the inventory with other people in the team
we need to accomplish two different activites

  - Stored the updated inventory info in a shared place
    in our case aws S3 bucket
  - Encrypt it with a common TeamAdmin key
    randomly generated and stored in a common
    Keepass or similar accessible only from the people<br>
    who as the grant to use and consume it.
   
    <strong>WARNING !!!!</strong><br>
      Keep in mind if you loose inventory password you are not<br>
      able to decrypt secrets,pem,ppk,inventory files related<br>
      to all environment created using automation in your account<br>
      Store it in a common secure place like Keepass<br>
      accessible from the other TeamAdmin in the project.<br>

# How work sharing inventory (by example)
Usually the playbook previously created
store all information related to the new environment in the infra/inventory folder.<br>
Now to enable encrypt and sharing we need
to improve it using an additional step.<br>

Example: <br>

In the playbook  [create_common_env.yml](../create_common_env.yml) you can see after tasks related to "setup ec2 env" the followed line 

```
- import_playbook: manage_inventory.yml
  vars:
    inventory_mode: pull
    project_inventory: "./inventory/{{ prj_name | regex_replace('_','/')}}/{{ prj_env }}/{{ prj_user }}"
```
The playbook imported **manage_inventory.yml** using the predifined
vars fetch all inventory files from S3 bucket
and store it locally and decrypt<br>
using ansible-vault tool.

At the end of the create_common_env.yml file you can see the same import_playbook  but with vars **inventory_mode: push** <br>
Doing this you are asking to encrypt the local inventory folder using ansible-vault and push it on s3 bucket.

```
- import_playbook: manage_inventory.yml
  vars:
    inventory_mode: push
    project_inventory: "./inventory/{{ prj_name | regex_replace('_','/')}}/{{ prj_env }}/{{ prj_user }}"
```


