# Deploy RDS Instance

<strong>Prerequisite:</strong>

To create an RDS instance you need to update properly the followed variables in default_and_convention.yml

  The followed variables are mandatory to tag properly the environment on cloud
   - prj_name: 'egm.eva' ({project}.{program}) 
   - prj_env: lch (spoc,dev,tst,int,acc,dlt,trn,lch)
   - prj_user: common01 (or the username related to the      user it will use the env)
  
  The followed variables defined the type of Db and size
   - rds_instance_type: db.t3.micro # who define the size of your instance 
   - rds_db_engine: oracle-se1 # the engine to use 
   - rds_allocated_storage: 50 # the size of the storage 

---
<strong>Create new RDS instance:</strong>

- Check or update rds variable in  default_convetions.yml file and execute:
```
  - cd infra; ansible-playbook create_rds.yml
```
  At the end fo the playbook all information related to the new instance it will be stored in in the file {prj_name/{prg_env}/{prj_name}-{prg_env}_rds.report.yml

  N.B. The creation require between 5 to 15 minutes
  so run the playbook and wait.

---
<strong>Delete RDS Instance</strong>

The playbook will take a snapshot of the RDS nad
delete instance security_group and local report_inventory file

 - Check or update rds variable in  default_convetions.yml file and execute:
```
  - cd infra; ansible-playbook delete_rds.yml
```
