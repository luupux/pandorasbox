---  
<br>

# Requirements:

## Permission you need 

- To work with automation you need:
  - TeamAdmin policy in Aws account 
  - To be a part of TeamAdmin group in AD in Aws
  - Secrets to use automation</br>

## Common Setup (valid for VDI and ADM-EGM)
    
  - Setup AWS secrets:
    To work with AWS you need to setup AWS_* env vars 
     Es:
     - bash command:
       - export AWS_ACCESS_KEY_ID='YOUR PERSONAL KEY'
       - export AWS_SECRET_ACCESS_KEY='YOUR KEY SECRET'
       - export AWS_REGION='eu-west-1' # needed for ec.py inventory
    - or you could set in  in ~/.bashrc
<br/>

---
 ## Work with ansible using ADM-EGM host
  To work from your ADM-EGM shell account you need to perform the followed task
  to run an ansible container ready to work with infra code and aws
  - clone cfd.devops
  - move in cfd.devops/utility/ansible folder
  - execute ./run_ansible
  - Now you are ready to follow the steps described in 
      - "Deploy ec2 instance with ansible automation"
<br/>

 ## Work with ansible using Amazon linux VDI
 
 - Install Ansible > 2.8 
   - setup AWS_SECRETS (common Setup described above)
   - sudo yum install python2-pip python2-passlib putty zip
   - sudo pip install bcrypt
   - sudo pip install boto 
   - sudo pip install boto3==1.12

 - Install putty and friends
   - Setup Epel repo
   - sudo yum-config-manager --enable epel
   - sudo yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
   - sudo yum install putty.x86_64
<br/>

## Setup inventory with ansible-vault (encryption inventory data on s3 bucket)

   - <strong>configure Inventory first time execution </strong>
   
     - execute openssl rand -base64 16 > ~/.vault_pass

      <strong>WARNING !!!!</strong><br>
      Keep in mind if you loose this password you are not<br>
      able to decrypt secrets,pem,ppk,inventory files related<br>
      to all environment created using automation in your account<br>
      So store it in a common secure place like Keepass<br>
      accessible from the other TeamAdmin in the project.<br>
   
   - <strong>configure your local inventory with password previously created</strong><br>
      only needed when you onboarding new teamadmin guy 

     - Ask to your team admin the vault password and save
       it in ~/.vault_pass

   - <strong>Additional information about inventory</strong>
     - [explantion about inventory and ansible-vault ](infra/docs/inventory.md)
<br/>

---
# Howto's

- [How to deploy Ec2 instance with docker preconfigured](infra/docs/ec2_docker.md)
- [How to deploy RDS instance](infra/docs/rds.md)
