# Aws_reports

The purpose is to generate a confluence page based
on aws service consumed (Ec2,Workspaces,Etc)

---

## Configuration:
### Requirements:
 - atlassian-python-api:1.16.0
 - pandas:0.24.2
 - collections


### Auth:
To work the script need to found the followed env variable

For Confluence you need to define:
 - export confluence_user='XXX'
 - export confluence_pass='XXX'
For aws:
 - export AWS_ACCESS_KEY_ID='XXX'
 - export AWS_SECRET_ACCESS_KEY='XXX'
 - export AWS_REGION=eu-west-1
--- 


## How to use it


Pull data from Aws (Ec2/Workspace) and push in a confluence page

```
 ./get_inventory.py
```

