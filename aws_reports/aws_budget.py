#!/usr/bin/python
import botocore
import boto3
import pandas as pd
from collections import MutableMapping
import datetime
# import pprint as pp


class aws_budget(object):
    def __init__(self):
        pd.set_option('display.max_rows', None)     # it's use only to show all columns in print debug
        pd.set_option('display.max_columns', None)  # ''
        pd.set_option('display.width', None)        # ''
        pd.set_option('display.max_colwidth', -1)   # ''
        self.prefix_bg_name = 'aaa_'
        self.defaultcols = ["BudgetName",
                            "CalculatedSpend_ActualSpend_Amount",
                            "BudgetLimit_Amount",
                            "CalculatedSpend_ForecastedSpend_Amount",
                            "Current vs Budgeted",
                            "Forecasted vs Budgeted",
                            "LastUpdatedTime",
                            "TimePeriod_Start",
                            "BudgetType",
                            "TimeUnit",
                            ]
        self.defaultlabels = ["BudgetName",
                              "Current",
                              "Budgeted",
                              "Forecasted",
                              "Current vs Budgeted",
                              "Forecasted vs Budgeted",
                              "LastUpdatedTime",
                              "TimePeriod_Start",
                              "BudgetType",
                              "TimeUnit",
                              ]
        self.table_style = [{
            'selector': 'table, th, td',
            'props': [('border', '1px solid #172b4d'),
                      ('border-collapse', 'collapse'),
                      ('font-size', '12'),
                      ('font-family', 'sans-serif'),
                      ('color', '#172b4d'),
                      ('backgroud-color', '#eee')
                      ]},
            {'selector': 'th, td', 'props': [('padding', '0 5px 0 5px')]},
            {'selector': 'th',
             'props': [('text-align', 'left'),
                       ('backgroud-color', '#eee')
                       ]}
        ]

    def __dict_flatten(self, d, parent_key='', sep='_'):
        items = []
        for k, v in d.items():
            new_key = parent_key + sep + k if parent_key else k
            if isinstance(v, MutableMapping):
                items.extend(self.__dict_flatten(v, new_key, sep=sep).items())
            else:
                items.append((new_key, v))
        return dict(items)

    def set_session(self, access_key, secret_key, account_name):
        self.aws_session = boto3.Session(aws_access_key_id=access_key,
                                         aws_secret_access_key=secret_key)
        self.accountid = self.aws_session.client('sts').get_caller_identity().get('Account')
        try:
            self.account_name = self.aws_session.client('iam').list_account_aliases()['AccountAliases'][0]
        except Exception:
            self.account_name = account_name
        check_bgt = self.__create()
        if check_bgt is None:
            return None

    def fetch_budgets(self):
        self.__create()

    def __create(self):
        print(self.account_name, self.accountid)
        self.b_client = self.aws_session.client('budgets')
        b_paginator = self.b_client.get_paginator('describe_budgets')
        self.b_result = b_paginator.paginate(AccountId=self.accountid).build_full_result()
        if len(self.b_result) == 0:
            return self.b_result
        self.bgts = self.b_result['Budgets']
        self.bgts_list = []
        for row in self.bgts:
            flatten = self.__dict_flatten(row)
            self.bgts_list.append(flatten)
        return(self.bgts_list)

    def html_budget(self, htmlfile='/tmp/budgets_table.html', header_labels='default', filtercols='default', htmltitle='default'):
        if len(self.b_result) == 0:
            self.bgts_list = self.b_result
            # 'No Budgets reports'
        return self.__createhtml(
            df=self.bgts_list,
            htmlfile=htmlfile,
            filtercols=filtercols,
            htmltitle=htmltitle,
            sortkey='BudgetName')

    def __forecast_vs_bgt(self, forecast, bgt):
        result = ((forecast.astype(float) / bgt.astype(float)) * 100)
        return result

    def __current_vs_bgt(self, current, bgt):
        result = (((current.astype(float) - bgt.astype(float)) / bgt.astype(float)) * 100 + 100)
        return result

    def __highlight_cols(self, s):
        if float(s) > 90:
            color = '#ff6700'
        else:
            color = '#172b4d'
        return 'color: %s' % color

    def __createhtml(self, df, htmlfile='/tmp/budget_table.html', filtercols='default',
                     header_labels='default', htmltitle='default', sortkey='default'):
        ''' create html file from dataframe'''
        self.sortkey = sortkey
        self.htmltitle = htmltitle
        self.htmlfile = htmlfile
        self.df = df
        if filtercols == 'default':
            self.filtercols = self.defaultcols
        else:
            self.filtercols = filtercols
        if header_labels == 'default':
            df_labels = self.defaultlabels
        else:
            df_labels = header_labels
        if htmltitle == 'default':
            self.htmltitle = '<h1>Report Table</h1>'
        else:
            self.htmltitle = htmltitle

        # Work with dataframe
        df = pd.DataFrame(self.df, columns=self.filtercols).sort_values(self.sortkey, ascending=True)
        df.columns = df_labels  # rename the column headers
        # df.columns = self.defaultlabels  # rename the column headers
        df['TimePeriod_Start'] = pd.to_datetime(df['TimePeriod_Start']).dt.strftime('%Y-%m-%d')  # format date
        df['LastUpdatedTime'] = pd.to_datetime(df['LastUpdatedTime']).dt.strftime('%Y-%m-%d')  # format date
        df['Forecasted vs Budgeted'] = self.__forecast_vs_bgt(df['Forecasted'], df['Budgeted'])
        df['Current vs Budgeted'] = self.__current_vs_bgt(df['Current'], df['Budgeted'])
        df['Current'] = df['Current'].astype(float).map('${:,.2f}'.format)  # format currency
        df['Budgeted'] = df['Budgeted'].astype(float).map('${:,.2f}'.format)  # format currency
        df['Forecasted'] = df['Forecasted'].astype(float).map('${:,.2f}'.format)  # format currency

        html_result = (df
                       .style.applymap(self.__highlight_cols, subset=pd.IndexSlice[:, ['Forecasted vs Budgeted']])
                       .set_table_styles(self.table_style)
                       .format({'Forecasted vs Budgeted': "{:,.2f}%"})
                       .format({'Current vs Budgeted': "{:,.2f}%"})
                       .hide_index()
                       .render()
                       )
        html_table = self.htmltitle + html_result
        html_f = open(self.htmlfile, 'w')
        html_f.write(html_table)
        html_f.close()
        return html_table

# bunch of functions to setup budgets dict
    def set_bg_tags(self, tags, tag_name='Project'):
        self.bg_tags = ['user:'+s.split(':')[0].strip() + '$' + s.split(':')[1].strip() for s in tags if s != 'nan']
        return(self.bg_tags)

    def set_bg_limit(self, amount, Unit='USD'):
        self.bg_limit = {'Amount': amount, 'Unit': Unit}

    def set_bg_timeperiod(self, start, end=None):
        self.bg_timeperiod = {}
        self.bg_timeperiod['Start'] = datetime.datetime.strptime(start, '%Y-%m-%d')
        if end is not None:
            self.bg_timeperiod['End'] = datetime.datetime.strptime(end, '%Y%m%d')

    def __check_budget(self):
        try:
            # result = self.b_client.describe_budget(AccountId=self.accountid, BudgetName=self.bg_name)
            self.b_client.describe_budget(AccountId=self.accountid, BudgetName=self.bg_name)
            return 'exist'
        except botocore.exceptions.ClientError:
            return 'notexist'

    def remove_old_budgets(self, budget_listed, prefix='auto_'):
        registered_bgts = list([prefix + x['budget_name'] for x in budget_listed])
        if len(self.b_result) > 0:
            aws_bgts = list([x['BudgetName'].encode("ascii") for x in self.b_result['Budgets'] if x['BudgetName'].startswith(prefix)])
            bgt_todelete = list(set(aws_bgts) - set(registered_bgts))
            for x in bgt_todelete:
                print(self.accountid, 'bgt_to_delete:', x)
                self.b_client.delete_budget(AccountId=self.accountid, BudgetName=x)
            return(bgt_todelete)

    def manage_notify(self, budget_name, topic_addr, mode='create'):
        notificationtype = 'ACTUAL'  # ACTUAL'|'FORECASTED'
        comparisonoperator = 'GREATER_THAN'  # 'GREATER_THAN'|'LESS_THAN'|'EQUAL_TO'
        threshold = float('90')
        thresholdtype = 'PERCENTAGE'  # 'PERCENTAGE'|'ABSOLUTE_VALUE'
        subscriptiontype = 'SNS'  # 'SNS'|'EMAIL'
        notificationstate = 'ALARM'  # 'OK'|'ALARM'

        notification = {
            'NotificationType': notificationtype,
            'ComparisonOperator': comparisonoperator,
            'Threshold': threshold,
            'ThresholdType': thresholdtype,
            'NotificationState': notificationstate
        }
        subscribers = [{
            'SubscriptionType': subscriptiontype,
            'Address': topic_addr
        }]
        if mode == 'delete':
            print('notify_to_delete', self.accountid, budget_name)
            self.b_client.delete_notification(AccountId=self.accountid,
                                              BudgetName=budget_name,
                                              Notification=notification)
        else:
            try:
                self.b_client.create_notification(AccountId=self.accountid,
                                                  BudgetName=budget_name,
                                                  Notification=notification,
                                                  Subscribers=subscribers)
            except botocore.exceptions.ClientError:
                print('Notification present for budget', budget_name)
                # print(ex)

    # main function who check the status of the bugdgets and create/update/delete it
    def manage_budget(self, bg_name, notify_topic='NONE', bg_type='COST',
                      bg_timeunit='MONTHLY', prefix='auto_'):
        self.bg_name = prefix + bg_name
        self.bg_timeunit = bg_timeunit
        self.bg_type = bg_type
        tg_dict = {}
        if len(self.bg_tags) > 0:
            tg_dict = {'TagKeyValue': self.bg_tags}
        self.budget_setup = {
            'BudgetName': self.bg_name,
            'BudgetLimit': self.bg_limit,
            'TimePeriod': self.bg_timeperiod,
            'TimeUnit': self.bg_timeunit,
            'BudgetType': self.bg_type,
            'CostFilters': tg_dict,
        }
        check_budget = self.__check_budget()
        if check_budget == 'notexist':
            print(self.accountid, 'create budget', self.bg_name)
            self.b_client.create_budget(AccountId=self.accountid, Budget=self.budget_setup)
        else:
            print(self.accountid, 'update budget', self.bg_name)
            self.b_client.update_budget(AccountId=self.accountid, NewBudget=self.budget_setup)
        if notify_topic != 'NONE':
            self.manage_notify(topic_addr=notify_topic, budget_name=self.bg_name)

    # fetch specific budget with flatten result
    def get_budget(self, bg_name):
        for ar in self.b_result['Budgets']:
            if ar['BudgetName'] == bg_name:
                ar['AccountName'] = self.account_name
                flatten = self.__dict_flatten(ar)
                return(flatten)

    # save specific budget in a df
    def collect_budgets(self, bg_name):
        try:
            len(self.clt_bgts)
        except Exception:
            self.clt_bgts = []
        self.customcols = ['AccountName'] + self.defaultcols
        ar = self.get_budget(bg_name)
        self.clt_bgts.append(ar)
        pd.DataFrame(self.clt_bgts, columns=self.customcols)

    # create html table for filtered budget
    def html_collected_budgets(self, htmlfile='/tmp/collected_budgets_table.html', htmltitle='default'):
        if len(self.b_result) == 0:
            self.bgts_list = self.b_result
        header_labels = ['AccountName'] + self.defaultlabels
        return self.__createhtml(
            df=self.clt_bgts,
            htmlfile=htmlfile,
            header_labels=header_labels,
            filtercols=self.customcols,
            htmltitle=htmltitle,
            sortkey='BudgetName')
