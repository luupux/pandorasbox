#!/usr/bin/python
import os
import boto3
import pandas as pd
# import json
# import pprint as pp


class inventory_ec2_ws(object):
    def __init__(self):
        self.css_table = '''
            <style>
            table, th, td {
                border: 1px solid #172b4d;
                border-collapse: collapse;
                font-size: 14;
                font-family: sans-serif;
                    color: #172b4d;
            }
            th, td { padding: 5px;}
            th {
                text-align: left;
                background-color: #eee;
            }
            </style>
            '''

    def set_session(self, access_key, secret_key, account_name):
        self.aws_session = boto3.Session(aws_access_key_id=access_key,
                                         aws_secret_access_key=secret_key)
        self.account_name = account_name

    def get_accountinfo(self):
        self.AccountId = self.aws_session.client('sts').\
                get_caller_identity().get('Account')
        try:
            self.account_name = self.aws_session.client('iam').\
                list_account_aliases()['AccountAliases'][0]
        except Exception:
            print('Iam not supported')
        return({'id': self.AccountId, 'name': self.account_name})

    def fetch_rds(self):
        self.defaultcols = ["DBName",
                            "Project",
                            "Environment",
                            "DBInstanceClass",
                            "DBInstanceIdentifier",
                            "LicenseModel",
                            "Engine",
                            "EngineVersion",
                            "DBInstanceStatus",
                            "Address",
                            "Port",
                            "StorageEncrypted",
                            "AllocatedStorage",
                            "PubliclyAccessible"]
        client = boto3.client('rds')
        paginator = client.get_paginator('describe_db_instances')
        self.page_dict = {}
        for page in paginator.paginate():
            self.page_dict.update(page)
        self.rdslist = []
        for dbi in self.page_dict['DBInstances']:
            self.rdsdict = {}
            # pprint.pprint(dbi)
            self.rdsdict.\
                update({'DBName': dbi['DBName']})
            self.rdsdict.\
                update({'DBInstanceClass': dbi['DBInstanceClass']})
            self.rdsdict.\
                update({'DBInstanceIdentifier': dbi['DBInstanceIdentifier']})
            self.rdsdict.\
                update({'LicenseModel': dbi['LicenseModel']})
            self.rdsdict.\
                update({'Engine': dbi['Engine']})
            self.rdsdict.\
                update({'EngineVersion': dbi['EngineVersion']})
            self.rdsdict.\
                update({'DBInstanceStatus': dbi['DBInstanceStatus']})
            self.rdsdict.\
                update({'MasterUsername': dbi['MasterUsername']})
            self.rdsdict.\
                update({'Address': dbi['Endpoint']['Address']})
            self.rdsdict.\
                update({'Port': dbi['Endpoint']['Port']})
            self.rdsdict.\
                update({'StorageEncrypted': dbi['StorageEncrypted']})
            self.rdsdict.\
                update({'AllocatedStorage': dbi['AllocatedStorage']})
            self.rdsdict.\
                update({'PubliclyAccessible': dbi['PubliclyAccessible']})
            self.rdsdict.\
                update({'DBInstanceArn': dbi['DBInstanceArn']})
            tags = client.list_tags_for_resource(
                ResourceName=dbi['DBInstanceArn'])['TagList']
            ltags = self.__list2dict(tags)
            self.rdsdict.update(ltags)
            self.rdslist.append(self.rdsdict)

    def __get_ws_bundle(self, Owner='null'):
        self.op_param = {}
        if Owner != 'null':
            self.op_param = {'Owner': Owner}
        client = boto3.client('workspaces')
        paginator = client.get_paginator('describe_workspace_bundles')
        page_iter = paginator.paginate(**self.op_param)
        all_bundles = []
        for page in page_iter:
            all_bundles = all_bundles + page['Bundles']
        self.bundle_list = []
        for row in all_bundles:
            self.new_row = {}
            for k in row:
                new_key = 'bundle_' + k
                self.new_row[new_key] = row[k]
            self.bundle_list.append(self.new_row)
        return self.bundle_list

    def fetch_ws(self):
        own_bundle = self.ws_bundles = self.__get_ws_bundle()
        amazon_bundle = self.ws_bundles = self.__get_ws_bundle(Owner='AMAZON')
        self.ws_bundles = own_bundle + amazon_bundle
        client = boto3.client('workspaces')
        paginator = client.get_paginator('describe_workspaces')
        pages = paginator.paginate()
        self.wslist = []
        for page in pages:
            for ws in page['Workspaces']:
                tags = client.describe_tags(ResourceId=ws['WorkspaceId'])
                wstags = tags['TagList']
                for tag in wstags:
                    ws.update({tag['Key']: tag['Value']})
                wsproperties = ws['WorkspaceProperties']
                ws.pop('WorkspaceProperties')
                ws.update(wsproperties)
                bundle_id = ws['BundleId']
                my_bundle = next(
                    (item for item in self.ws_bundles
                     if item['bundle_BundleId'] == bundle_id), [])
                ws.update(my_bundle)
                self.wslist.append(ws)
                # pp.pprint(ws)

        self.defaultcols = ["UserName",
                            "Name",
                            "Project",
                            "Environment",
                            "State",
                            "ComputeTypeName",
                            "ComputerName",
                            "RunningMode",
                            "WorkspaceId",
                            "bundle_Name",
                            "bundle_Owner"]

        self.css_table = '''
            <style>
            table, th, td {
                border: 1px solid #172b4d;
                border-collapse: collapse;
                font-size: 12;
                font-family: sans-serif;
                    color: #172b4d;
            }
            th, td { padding: 5px;}
            th {
                text-align: left;
                background-color: #eee;
            }
            </style>
            '''

    def __createhtml(self, df, htmlfile='/tmp/Table.html',
                     filtercols='default', htmltitle='default'):
        ''' create html file from dataframe'''
        self.htmltitle = htmltitle
        self.htmlfile = htmlfile
        self.df = df
        if filtercols == 'default':
            self.filtercols = self.defaultcols
        else:
            self.filtercols = filtercols

        if htmltitle == 'default':
            self.htmltitle = '<h1>Report Table</h1>'
        else:
            self.htmltitle = htmltitle

        df = pd.DataFrame(self.df, columns=self.filtercols).\
            sort_values("Project", ascending=True)
        html_table = self.css_table + self.htmltitle + \
            df.to_html(index_names=False, index=False)
        html_f = open(self.htmlfile, 'w')
        html_f.write(html_table)
        html_f.close()
        return html_table

    def __createcsv(self, df, path='/tmp',
                    csvfile='default.csv', filtercols='default'):
        ''' create csv file from dataframe'''
        c_file = self.AccountId + '_' + csvfile
        self.csvfile = os.path.join(path, c_file)
        if os.path.isfile(self.csvfile):
            os.remove(self.csvfile)
        self.df = df
        if filtercols == 'default':
            self.filtercols = self.defaultcols
        else:
            self.filtercols = filtercols
        df = pd.DataFrame(self.df, columns=self.filtercols).\
            sort_values("Project", ascending=True)
        df.to_csv(self.csvfile, index=False)
        return self.csvfile

    def __list2dict(self, lst):
        op = {}
        for row in lst:
            op.update({row['Key']: row['Value']})
        return op

    def fetch_ec2(self):
        self.defaultcols = ["Name",
                            "Project",
                            "Environment",
                            "KeyName",
                            "ec-scheduler",
                            "InstanceType",
                            "VpcId",
                            "InstanceId"]
        self.ec2list = []
        ec2 = boto3.client('ec2')
        paginator = ec2.get_paginator('describe_instances')
        self.page_dict = {}
        for page in paginator.paginate():
            self.page_dict.update(page)
        for a in (self.page_dict['Reservations']):
            for b in a['Instances']:
                self.ec2dict = {}
                self.ec2tags = self.__list2dict(b['Tags'])
                self.ec2dict.update(self.ec2tags)
                self.ec2dict.update({'LaunchTime': b['LaunchTime']})
                self.ec2dict.update({'InstanceId': b['InstanceId']})
                self.ec2dict.update({'InstanceType': b['InstanceType']})
                # self.ec2dict.update({'KeyName': b['KeyName']})
                try:
                    self.ec2dict.update({'KeyName': b['KeyName']})
                except Exception:
                    KeyName = self.ec2tags['Name']
                    self.ec2dict.update({'KeyName': KeyName})
                self.ec2dict.update({'State': b['State']['Name']})
                # self.ec2dict.update({'SecurityGroup':
                #                       b['SecurityGroups'][0]['GroupName']})
                self.ec2dict.update({'VpcId': b['VpcId']})
                self.ec2list.append(self.ec2dict)  # append in final list

    def csv_rds(self, path='tmp',
                csvfile='rds_report.csv', filtercols='default'):
        return self.__createcsv(df=self.rdslist, csvfile=csvfile,
                                filtercols=filtercols)

    def html_rds(self, htmlfile='/tmp/rds_table.html',
                 filtercols='default', htmltitle='default'):
        return self.__createhtml(df=self.rdslist, htmlfile=htmlfile,
                                 filtercols=filtercols, htmltitle=htmltitle)

    def csv_ec2(self, path='/tmp',
                csvfile='ec2_report.csv', filtercols='default'):
        return self.__createcsv(df=self.ec2list, csvfile=csvfile,
                                filtercols=filtercols)

    def html_ec2(self, htmlfile='/tmp/ec2_table.html',
                 filtercols='default', htmltitle='default'):
        return self.__createhtml(df=self.ec2list, htmlfile=htmlfile,
                                 filtercols=filtercols, htmltitle=htmltitle)

    def csv_workspace(self, path='/tmp',
                      csvfile='workspace_report.csv', filtercols='default'):
        return self.__createcsv(df=self.wslist, csvfile=csvfile,
                                filtercols=filtercols)

    def html_workspace(self, htmlfile='/tmp/workspace_table.html',
                       filtercols='default', htmltitle='default'):
        return self.__createhtml(df=self.wslist, htmlfile=htmlfile,
                                 filtercols=filtercols, htmltitle=htmltitle)

# a = inventory_ec2_ws()
# a.fetch_ec2()
# a.csv_ec2()
# res = a.html_ec2(htmlfile='ec2.html')
# print(res)
# a.fetch_ws()
# a.html_workspace(htmlfile='workspace.html')
# # a.csv_workspace()
