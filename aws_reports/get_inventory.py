#!/usr/bin/python
# import pprint as pp
import os

from inventory_ec2_ws import inventory_ec2_ws
from conflu_table import conflu_table
from aws_budget import aws_budget
from modules.aws.aws_auth import Aws_session
from modules.aws.aws_sns import Aws_sns


CONFLU_USER = os.environ['CONFLU_USER']
CONFLU_PASS = os.environ['CONFLU_PASS']
CONFLU_URL = os.environ['CONFLU_URL']
CONFLU_SPACE = os.environ['CONFLU_SPACE']
CONFLU_PARENTPAGE = os.environ['CONFLU_PARENTPAGE']
AWS_ACCOUNT_NAME = os.environ['AWS_ACCOUNT_NAME']

# Full Page
full_report = inventory_ec2_ws()
full_report.set_session(access_key=os.environ['AWS_ACCESS_KEY_ID'],
                        secret_key=os.environ['AWS_SECRET_ACCESS_KEY'],
                        account_name=os.environ['AWS_ACCOUNT_NAME'])
accinfo = full_report.get_accountinfo()
accid = accinfo['id']

# Budget
budgets = aws_budget()
budgets.set_session(access_key=os.environ['AWS_ACCESS_KEY_ID'],
                    secret_key=os.environ['AWS_SECRET_ACCESS_KEY'],
                    account_name=os.environ['AWS_ACCOUNT_NAME'])

# Generate Bugets report base on confluence config page
page_conf = {
  'url': CONFLU_URL,
  'username': CONFLU_USER,
  'password': CONFLU_PASS,
  'space': CONFLU_SPACE,
  'parent_page': CONFLU_PARENTPAGE,
  'title': "Full Report " + AWS_ACCOUNT_NAME + " ("+accid+")",
  'body': ' '
}

setup_page = 'Setup-'+accid
# setup_page='Setup-test-'+accid # REMOVE IT AFTER TESTING
conf_page = conflu_table(page_conf)
budget_config = conf_page.get_conf(space=CONFLU_SPACE,
                                   page_title=CONFLU_PARENTPAGE,
                                   setup_page=setup_page)

b = Aws_session()
b.set_session(access_key=os.environ['AWS_ACCESS_KEY_ID'],
              secret_key=os.environ['AWS_SECRET_ACCESS_KEY'],
              account_name=os.environ['AWS_ACCOUNT_NAME'])
auth_session = b.get_accountinfo()
s = Aws_sns(auth_session=auth_session)

for bg_cfg in budget_config:
    budgets.set_bg_tags(tags=bg_cfg['tags'])
    budgets.set_bg_limit(amount=bg_cfg['budget_budgeted'])
    budgets.set_bg_timeperiod(start=bg_cfg['budget_timeperiod_start'])
    # create sns topic if there are reporters mails in confluence
    topic_arn = 'NONE'
    if len(bg_cfg['reporters']) > 0:
        topic_arn = s.create_topic(bg_cfg['budget_name'])
        emails = bg_cfg['reporters']
        s.subscribe_emails(topic_name=bg_cfg['budget_name'], emails=emails)
    budgets.manage_budget(bg_cfg['budget_name'], notify_topic=topic_arn)

# Remove old budget from aws
bgt_deleted = budgets.remove_old_budgets(budget_config)

# Remove sns topic related to budget
if (len(bgt_deleted)):
    for bgt in bgt_deleted:
        s.delete_topic(bgt)

# Genarete html fresh budgets page
budgets.fetch_budgets()
budget_report = budgets.\
    html_budget(htmltitle='<h1>Budgets list without feed</h1>')

########
# rds
full_report.fetch_rds()
rds_result = full_report.html_rds(htmltitle='<h1>Rds List</h1>')
csv_rds = full_report.csv_rds()

# ec2
full_report.fetch_ec2()
ec2_result = full_report.html_ec2(htmltitle='<h1>Ec2 List</h1>')
csv_ec2 = full_report.csv_ec2()

# ws_result = ec2_result
# ws
full_report.fetch_ws()
ws_result = full_report.html_workspace(htmltitle='<h1>WorkSpaces List</h1>')
csv_vdi = full_report.csv_workspace()

# Generate Full Report conflu_page
page_info = {
  'url': CONFLU_URL,
  'username': CONFLU_USER,
  'password': CONFLU_PASS,
  'space': CONFLU_SPACE,
  'parent_page': CONFLU_PARENTPAGE,
  'title': "Full Report " + AWS_ACCOUNT_NAME + " ("+accid+")",
  'body': budget_report + ws_result + ec2_result + rds_result
}

fullpage = conflu_table(page_info)
fullpage.managepage()

# Generate Full Report csv
page_info = {
  'url': CONFLU_URL,
  'username': CONFLU_USER,
  'password': CONFLU_PASS,
  'space': CONFLU_SPACE,
  'parent_page': CONFLU_PARENTPAGE,
  'title': "CSV attachments " + AWS_ACCOUNT_NAME + " ("+accid+")",
  'body': '<h1>List of attachments</h1>'
}

fullpage = conflu_table(page_info)
fullpage.managepage()
fullpage.attach_file(filename=csv_rds)
fullpage.attach_file(filename=csv_ec2)
fullpage.attach_file(filename=csv_vdi)
fullpage.build_attach_page()
