#!/usr/bin/python
import pprint as pp

from inventory_ec2_ws import inventory_ec2_ws
from conflu_table import conflu_table
from aws_budget import aws_budget
import os
import re
CONFLU_USER = os.environ['CONFLU_USER']
CONFLU_PASS = os.environ['CONFLU_PASS']
CONFLU_URL = os.environ['CONFLU_URL']
CONFLU_SPACE = os.environ['CONFLU_SPACE']
CONFLU_PARENTPAGE = os.environ['CONFLU_PARENTPAGE']
AWS_ACCOUNT_NAME = os.environ['AWS_ACCOUNT_NAME']

def set_env(conf):
    with open(conf) as f:
        for line in f:
            
            if 'export' not in line:
                continue
            if line.startswith('#'):
                continue
            if line.startswith('export AWS_'):
                key, value = line.replace('export ', '', 1).strip().split('=', 1)
                os.environ[key] = value

p = re.compile('^[0-9].-')
pathconf= '/home/fermaur/KEYS/aws_profiles/'
ldir = (os.listdir(pathconf))

conflist = sorted([ os.path.join(pathconf, s) for s in ldir if p.match(s)])
budgets = aws_budget()
for conf in conflist:
    set_env(conf)
    #os.system("env |grep CONFLU")
    #os.system("env |grep AWS")
    budgets.set_session(access_key=os.environ['AWS_ACCESS_KEY_ID'], 
                        secret_key=os.environ['AWS_SECRET_ACCESS_KEY'],
                        account_name=os.environ['AWS_ACCOUNT_NAME'])
    budgets.collect_budgets('auto_Global Budget')

# generate html table 
html_budgets = budgets.html_collected_budgets(htmltitle='<h1>Budgets list without feed</h1>')

# Generate final conflu_page
page_info = {
  'url': CONFLU_URL,
  'username': CONFLU_USER,
  'password': CONFLU_PASS,
  'space': CONFLU_SPACE,
  'parent_page': CONFLU_PARENTPAGE,
  'title': "Report collected budgets" ,
  'body': html_budgets
}

fullpage = conflu_table(page_info)
fullpage.managepage()