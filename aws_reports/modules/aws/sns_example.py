#!/usr/bin/python
import os
import re
import pprint as pp
from modules.aws.aws_auth import Aws_session
from modules.aws.aws_sns import Aws_sns


CONFLU_USER = os.environ['CONFLU_USER']
CONFLU_PASS = os.environ['CONFLU_PASS']
CONFLU_URL = os.environ['CONFLU_URL']
CONFLU_SPACE = os.environ['CONFLU_SPACE']
CONFLU_PARENTPAGE = os.environ['CONFLU_PARENTPAGE']
AWS_ACCOUNT_NAME = os.environ['AWS_ACCOUNT_NAME']


b = Aws_session()
b.set_session(access_key=os.environ['AWS_ACCESS_KEY_ID'], 
              secret_key=os.environ['AWS_SECRET_ACCESS_KEY'],
              account_name=os.environ['AWS_ACCOUNT_NAME'])
auth_session = b.get_accountinfo()

# define sns 
s = Aws_sns(auth_session=auth_session)
s.fetch_list_topics()
topic_name='fermaur_topic_test'
s.create_topic(topic_name)
#s.subscriptions_by_topic('fermaur_topic_test')
#s.subscriptions_by_topic('S4D-ADmon-ADcon-B4')
emails=['luupux@gmail.com','luupuxall@gmail.com' ]
s.subscribe_emails(topic_name=topic_name,emails=emails)
emails=['ciccio@plutto.it','luupuxall@gmail.com' ]
c
