#!/usr/bin/python
import boto3


class Aws_session(object):
    def set_session(self, **kwargs):
        self.aws_session = boto3.Session(aws_access_key_id=kwargs['access_key'],
                                         aws_secret_access_key=kwargs['secret_key']
                                         )
        self.account_name = kwargs['account_name']

    def get_accountinfo(self):
        self.AccountId = self.aws_session.client('sts').get_caller_identity().get('Account')
        try:
            self.account_name = self.aws_session.client('iam').list_account_aliases()['AccountAliases'][0]
        except Exception as ex:
            print('Iam not supported', ex)
        print('Auth: ', self.AccountId, self.account_name)
        return({'id': self.AccountId, 'name': self.account_name, 'session': self.aws_session})


# import os
# import re
# CONFLU_USER = os.environ['CONFLU_USER']
# CONFLU_PASS = os.environ['CONFLU_PASS']
# CONFLU_URL = os.environ['CONFLU_URL']
# CONFLU_SPACE = os.environ['CONFLU_SPACE']
# CONFLU_PARENTPAGE = os.environ['CONFLU_PARENTPAGE']
# AWS_ACCOUNT_NAME = os.environ['AWS_ACCOUNT_NAME']

# b = Aws_session()
# b.set_session(access_key=os.environ['AWS_ACCESS_KEY_ID'],
#               secret_key=os.environ['AWS_SECRET_ACCESS_KEY'],
#               account_name=os.environ['AWS_ACCOUNT_NAME'])
# accinfo = b.get_accountinfo()
