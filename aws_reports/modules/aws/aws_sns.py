#!/usr/bin/python
import re
# import boto3
import pprint as pp


class Aws_sns(object):
    def __init__(self, auth_session):
        self.aws_session = auth_session['session']
        self.accountid = auth_session['id']
        self.b_client = self.aws_session.client('sns')
        self.topic_prefix = 'auto_budget_'

    def __fetch_topics(self):
        boto_method = 'list_topics'
        self.topic_list = []
        paginator = self.b_client.get_paginator(boto_method)
        for ar in paginator.paginate():
            for topic in ar['Topics']:
                self.topic_list.append(topic['TopicArn'])

    def __prefix_topic(self, topic_name):
        if topic_name.find('auto_') != -1:
            topic_name = topic_name.replace('auto_','')
        return (self.topic_prefix+topic_name).replace(" ", "_")

    def delete_topic(self, topic_name):
        self.__fetch_topics()
        topic_name = self.__prefix_topic(topic_name)
        src_topic = ':' + topic_name + '$'
        #print('src_topic',src_topic,'topic_name',topic_name)
        check_topic = [x for x in self.topic_list if re.search(src_topic, x)]
        if len(check_topic) > 0 :
            print('topic to delete',check_topic[0])
            res = self.b_client.delete_topic(TopicArn=check_topic[0])

    def create_topic(self, topic_name):
        self.__fetch_topics()
        topic_name = self.__prefix_topic(topic_name)
        src_topic = ':' + topic_name + '$'
        check_topic = [x for x in self.topic_list if re.search(src_topic, x)]
        if len(check_topic) == 0:
            msg = 'Create Topic: '
        else:
            msg = 'Topic Present: '
        res = self.b_client.create_topic(Name=topic_name)
        return res['TopicArn']

    def __fetch_subscr(self, topic_name):
        boto_method = 'list_subscriptions_by_topic'
        subscr_by_topic = []
        self.__fetch_topics()
        topic_name = self.__prefix_topic(topic_name)
        src_topic = ':'+topic_name+'$'
        topics_arn = [x for x in self.topic_list if re.search(src_topic, x)]
        for topic in topics_arn:
            op_param = {'TopicArn': topic}
            paginator = self.b_client.get_paginator(boto_method)
            for page in paginator.paginate(**op_param):
                subscr_by_topic.append(page['Subscriptions'])
        return subscr_by_topic[0]
    
    def subscribe_emails(self, emails, topic_name, clean_notpresent=True, protocol='email'):
        subscr_result = self.__fetch_subscr(topic_name=topic_name)
        topic_orig = topic_name
        topic_name = self.__prefix_topic(topic_name)
        src_topic = ':'+topic_name+'$'
        topic_arn = [x for x in self.topic_list if re.search(src_topic, x)]
        subscr_result_mail = [x['Endpoint'] for x in subscr_result]
        not_registered = list(set(emails) - set(subscr_result_mail))
        for email in not_registered:
            print('Register:', email, 'Topic:', topic_arn[0])
            op_param = {'TopicArn': topic_arn[0],
                        'Protocol': protocol,
                        'Endpoint': email,
                        'ReturnSubscriptionArn': True
                        }
            self.b_client.subscribe(**op_param)
        if clean_notpresent == True:
            tobe_deleted = list(set(subscr_result_mail) - set(emails))
            if len(tobe_deleted) > 0:
                print('tobe_deleted',tobe_deleted)
                self.unsubscribe_emails(topic_name=topic_orig, emails=tobe_deleted)

    def unsubscribe_emails(self, emails, topic_name, protocol='email'):
        subscr_result = self.__fetch_subscr(topic_name=topic_name)
        for email in emails:
            arns_toremove = [{'arn': x['SubscriptionArn'], 'email': email} for x in subscr_result if x['Endpoint'] == email]
        for subscr in arns_toremove:
            if subscr['arn'] == 'PendingConfirmation':
                print('In pending, delete not permitted', subscr['email'], subscr['arn'])
                continue
            print('Delete', subscr['email'], '--', subscr['arn'])
            op_param = {'SubscriptionArn': subscr['arn']}
            self.b_client.unsubscribe(**op_param)
