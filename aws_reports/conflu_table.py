#!/usr/bin/python
from atlassian import Confluence
import pandas as pd
# import json
# import pprint as pp


class conflu_table(object):
    def __init__(self, cdict):
        self.argf = cdict
        self.conflu = Confluence(
                    url=self.argf['url'],
                    username=self.argf['username'],
                    password=self.argf['password'])

    def createpage(self, parent_id='None'):
        res = self.conflu.create_page(space=self.argf['space'],
                                      title=self.argf['title'],
                                      body=self.argf['body'],
                                      parent_id=parent_id)
        return res

    def checkpage(self, space, title):
        res = self.conflu.get_page_by_title(space=space, title=title)
        if isinstance(res, dict):
            return res
        else:
            return False

    def updatepage(self, id, parent_id='None'):
        res = self.conflu.update_page(parent_id=parent_id,
                                      page_id=id,
                                      title=self.argf['title'],
                                      body=self.argf['body'])
        return res

    def __get_child_pages(self, space, page_title):
        page_id = self.conflu.get_page_id(space, page_title)
        result_list = self.conflu.get_child_pages(page_id=page_id)
        return result_list

    def get_conf(self, space, page_title, setup_page='Setup'):
        # fetch budget config from confluence and return list of dict
        result_list = self.__get_child_pages(space=space,
                                             page_title=page_title)
        cfg_dict = list(filter(
                        lambda st: st['title'] == setup_page, result_list))
        cfg_id = cfg_dict[0]['id']
        cfg_body = self.conflu.get_page_by_id(page_id=cfg_id,
                                              expand='body.storage')
        cfg_content = cfg_body['body']['storage']['value']
        cfg_list = pd.read_html(cfg_content)
        # it's use only to show all columns in print debug
        pd.set_option('display.max_rows', None)
        pd.set_option('display.max_columns', None)
        pd.set_option('display.width', None)
        pd.set_option('display.max_colwidth', -1)
        # end debug print
        df = pd.DataFrame(cfg_list)
        df1 = df[0][0].dropna(how='all')
        setup_conf = []
        for index, row in df1.iterrows():
            reporters = str(row['reporters']).split(',')
            if 'nan' in reporters:
                reporters.remove('nan')
            res = {
                'budget_name': str(row['budget_name']),
                'budget_budgeted': str(row['budget_budgeted']),
                'budget_timeperiod_start': str(row['budget_timeperiod_start']),
                'budget_type': str(row['budget_type']),
                'time_unit': row['time_unit'],
                'tags': str(row['tags']).split(','),
                'reporters1': str(row['reporters']).split(','),
                'reporters': reporters
            }
            setup_conf.append(res)
        return setup_conf

    def managepage(self):
        if 'parent_page' in self.argf.keys():
            print(self.argf['parent_page'])
            self.parent_result = self.checkpage(space=self.argf['space'],
                                                title=self.argf['parent_page'])
            # print self.parent_result
            self.parent_id = self.parent_result['id']
        else:
            self.parent_id = 'None'

        self.testpage = self.checkpage(space=self.argf['space'],
                                       title=self.argf['title'])
        if self.testpage is False:
            print("created page " + self.argf['title'])
            self.createpage(parent_id=self.parent_id)
        else:
            self.updatepage(self.testpage['id'], parent_id=self.parent_id)
            urlpage = self.argf['url']+self.testpage['_links']['webui']
            print("updated page " + urlpage)

    def attach_file(self, filename):
        self.filename = filename
        res = self.conflu.get_page_by_title(space=self.argf['space'],
                                            title=self.argf['title'])
        self.conflu.attach_file(self.filename, page_id=res['id'])

    def build_attach_page(self):
        rsp = self.conflu.get_page_by_title(space=self.argf['space'],
                                            title=self.argf['title'])
        pageid = rsp['id']
        res = self.conflu.get_attachments_from_content(pageid, start=0,
                                                       limit=50, expand=None,
                                                       filename=None,
                                                       media_type=None)
        html_lnk = ''
        for lnk in res.get('results'):
            fln = lnk['title']
            link = """<h2><p>
                <ac:link>
                <ri:attachment ri:filename="{}"/>
                </ac:link>
                </p></h2>""".format(fln)
            html_lnk = html_lnk + link
        self.conflu.append_page(pageid, self.argf['title'], html_lnk)
