import os
import sys

def get_script_path():
    return os.path.dirname(os.path.realpath(sys.argv[0]))

script_dir = get_script_path()

execfile(script_dir + '/commonfuncs.py')

# Security Real Name
security_realm_name= 'myrealm'
domain_name = os.environ.get('DOMAIN_NAME', 'base_domain')
ecas_mock_ip = os.environ.get('ECAS_IP', 'ecas-mock')
pre_class_path =  os.environ.get('PRE_CLASSPATH')
class_path =  os.environ.get('CLASSPATH')
post_class_path =  os.environ.get('POST_CLASSPATH')

print('domain_name           : [%s]' % domain_name);
print('security_realm_name   : [%s]' % security_realm_name);
print('ECAS mock IP          : [%s]' % ecas_mock_ip);
print('CLASSPATH             : [%s]' % class_path);
print('PRE_CLASSPATH         : [%s]' % pre_class_path);
print('POST_CLASSPATH        : [%s]' % post_class_path);


# Connect to the AdminServer
# ==========================
connectToAdmin()

# Create a ManagedServer
# ======================
editMode()

cd('/SecurityConfiguration/%s/Realms/myrealm' % domain_name)
cmo.createAuthenticationProvider('EcasIdentityAsserter', 'eu.cec.digit.ecas.client.j2ee.weblogic.EcasIdentityAsserterV2')
cd('/SecurityConfiguration/%s/Realms/myrealm/AuthenticationProviders/EcasIdentityAsserter' % domain_name)
cmo.setControlFlag('OPTIONAL')
cmo.setEcasServerDirectHostName(ecas_mock_ip)
cmo.setEcasBaseUrl('https://%s:7002' % ecas_mock_ip)

cmo.setEcasServerDirectHostName(ecas_mock_ip)
cmo.setEcasBaseUrl('https://'+ecas_mock_ip+':7002')
#cmo.setAcceptStrengths('BASIC,PASSWORD,PASSWORD_SMS,PASSWORD_TOKEN,CLIENT_CERT')
cmo.setAcceptStrengths('BASIC')
set('ExcludedContextPaths',jarray.array([String('/console/'), String('/console'), String('/wlscat'),String('/wlscat/'), String('/wls-cat'),String('/wls-cat/')], String))

cd('/SecurityConfiguration/%s/Realms/myrealm' % domain_name)
set('AuthenticationProviders',jarray.array([ObjectName('Security:Name=myrealmEcasIdentityAsserter'), ObjectName('Security:Name=myrealmDefaultAuthenticator'), ObjectName('Security:Name=myrealmDefaultIdentityAsserter')], ObjectName))

cd('/SecurityConfiguration/%s/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator' % domain_name)
cmo.setControlFlag('OPTIONAL')


activate()
disconnect()
exit()
