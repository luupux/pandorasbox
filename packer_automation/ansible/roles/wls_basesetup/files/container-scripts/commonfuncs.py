import os
import socket

# Variables
# =========
# Environment Vars
hostname       = socket.gethostname()
# Admin Vars
admin_username = os.environ.get('ADMIN_USERNAME', 'weblogic')
admin_password = os.environ.get('ADMIN_PASSWORD','weblogic1') # this is read only once when creating domain (during docker image build)
admin_host     = os.environ.get('ADMIN_HOST', 'localhost')
admin_port     = os.environ.get('ADMIN_PORT', '7001')
admin_name     = os.environ.get('ADMIN_NAME', 'AdminServer')
# Node Manager Vars
nmname         = os.environ.get('NM_NAME', 'Machine-' + hostname)

# Functions
def editMode():
    edit()
    startEdit(waitTimeInMillis=-1, exclusive="true")

def saveActivate():
    save()
    activate(block="true")

def connectToAdmin():
    #connect(url='t3://' + admin_host + ':' + admin_port, adminServerName='AdminServer')
    connect(username=admin_username, password=admin_password,  url='t3://' + admin_host + ':' + admin_port, adminServerName=admin_name)
