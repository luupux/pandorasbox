import os
import socket

def get_script_path():
    return os.path.dirname(os.path.realpath(sys.argv[0]))

script_dir = get_script_path()
execfile(script_dir + '/commonfuncs.py')

# Security Real Name
security_realm_name= 'myrealm'
domain_name = os.environ.get('DOMAIN_NAME', 'base_domain')

# Connect to the AdminServer
# ==========================
connectToAdmin()

# Create a ManagedServer
# ======================
editMode()


#edit()
#startEdit()


try:
  # enables digested username passwords

  cd('/')
  cmo.createWebserviceSecurity('default_wss')

  cd('/WebserviceSecurities/default_wss')
  cmo.createWebserviceCredentialProvider('credProvider')

  cd('/WebserviceSecurities/default_wss/WebserviceCredentialProviders/credProvider')
  cmo.setClassName('weblogic.xml.crypto.wss.UNTCredentialProvider')
  cmo.setTokenType('ut')

  ##
  cd('/WebserviceSecurities/default_wss/WebserviceCredentialProviders/credProvider')
  cmo.createConfigurationProperty('UsePasswordDigest')

  cd('/WebserviceSecurities/default_wss/WebserviceCredentialProviders/credProvider/ConfigurationProperties/UsePasswordDigest')
  cmo.setEncryptValueRequired(false)
  cmo.setValue('true')
  ##

  cd('/SecurityConfiguration/'+domain_name+'/Realms/'+security_realm_name+'/AuthenticationProviders/DefaultAuthenticator')
  cmo.setPasswordDigestEnabled(true)

  cd('/SecurityConfiguration/'+domain_name+'/Realms/'+security_realm_name+'/AuthenticationProviders/DefaultIdentityAsserter')
  set('ActiveTypes',jarray.array([String('AuthenticatedUser'), String('wsse:PasswordDigest')], String))

  activate()

except :
  print "Error while trying to enabling Digest username token"
  for errarg in sys.exc_info()[0:]:
    print '   ', errarg
  print "Canceling domain modifications"
  stopEdit('y')

disconnect()

exit()
