#!/bin/sh

set -e
function pause(){
       read -p "$*"
   }

#pause "Press [Enter] key to continue..."

#see if the database host is already mapped in hosts file via the command line, otherwise it will be added automatcally
USER=$(whoami)
if [ "${USER}" == "root" ]; then
    DATABASE_HOST=$(grep database /etc/hosts)
    if [  "" == "${DATABASE_HOST}"  ]; then
        HOST_ENTRY=$(echo "${DB_IP:-localhost} database")
        echo "\n${HOST_ENTRY}" >> /etc/hosts
        echo "Adding to /etc/etc the following entry: ${HOST_ENTRY}"
    fi
fi

RED='\033[0;31m'

EXTRA_LIB_DIR="${DOMAIN_HOME}/extralibs"

#if directory exist
if [[ -d "$EXTRA_LIB_DIR" ]]; then
#   copy each file
    FILES=$(ls $EXTRA_LIB_DIR)
    for file in $FILES; do
        cp -vf "${EXTRA_LIB_DIR}/${file}" "${DOMAIN_HOME}/lib/"
        chmod -v +r "${DOMAIN_HOME}/lib/${file}"
    done
fi

echo "Docker container initialised, ready to launch CMD [$@] as [${USER}] from $(pwd)"

#let CMD be fired
exec "$@"
