#!/bin/sh
echo starting weblogic in background...

DOMAIN_HOME=${DOMAINS_HOME}/${DOMAIN_NAME}
mkdir -p ${DOMAIN_HOME}/logs/ && touch ${DOMAIN_HOME}/logs/weblogic.output.log

sh ${DOMAIN_HOME}/bin/startWebLogic.sh noderby > ${DOMAIN_HOME}/logs/weblogic.output.log 2>&1 &

while true
do
        if grep -q 'The server started in RUNNING mode' "${DOMAIN_HOME}/logs/weblogic.output.log"; then
            echo "Weblogic started!"
            break
        else
            echo "Weblogic not started yet..."
        fi
  sleep 10
done
