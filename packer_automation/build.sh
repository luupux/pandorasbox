#/bin/bash
#set -x
build_role=$1
echo $build_role
if [ -z "$build_role" ]
then
  echo "please specify the folder"
else
   if [ ! -d "./$build_role" ]
   then
    echo "folder $build_role not exist"
    exit 1
   fi
fi
cd ./$build_role

# set add env_vars configuration
set -o allexport
source env_vars
set +o allexport
image_dist=$(echo $image_src| cut -f1 -d : )
image_ver=$(echo $image_src| cut -f2 -d : )
export image_dist=$image_dist
export image_ver=$image_ver
env |grep image
env |grep build
env |grep DOMAIN
env |grep ECAS_BINARY

# export git info
git_commit=$(git log -1 --format=%h)
export git_commit
git_branch=$(git rev-parse --abbrev-ref HEAD)
export git_branch

#setup ansible env vars
export  ANSIBLE_ROLES_PATH=${PWD}/../ansible/roles

#setup login in regitry (FIX ME USING DIRECT PACKER
check_login=$(cat ~/.docker/config.json |grep -o $registry_host)
if [ -z "$check_login" ]
then
  echo $registry_pass |docker login -u $registry_user  --password-stdin $registry_host
fi


packer build packer_template.json 
