
# What is this 
The scope of this repo is to collect all ansible playbook useful to build a docker image using hashicorp packer.
<br><br>

# How it work 
The idea is to use a packer template to build 
a docker image using ansible playbook instead of using
dockerfile plus bash script, and push it directly in 
the registry.
<br><br>

# Requirements 
 - ec2 with linux 
 - Hashicorp Packer (https://www.packer.io/)
 - setup  env variable for registry (nexus) auth :
   to do thad you need to export this variable with proper value
   - export registry_user='FILL ME'
   - export registry_pass='FILL ME'
   - export registry_host='FILL ME:WITH PORT NUMBER'
<br><br>
# Usage
## How to build an image base on existing packer template 

  Syntax: 
   - ### **./build.sh** name_of_the_folder_image 
  Example:
   - ### ./build.sh **java_jdk** to build jdk docker image 
   - ### ./build.sh **java_jre** to build jre docker image 
   - ### ./build.sh **centos** to build centos docker image 
  
  **At the end of the execution you will have your new image
  in folder automation/$env_target in your $registry_host** 

<br><br>
# How to new  custom build image
## First to go: 
### Explanation about folders and scripts: #
 - **build.sh script:** is the entry point to build an image.
 - **ansible: folder** Who contain all shared roles who you can use in 
  any building  container process.
- an **java_jdk,centos,etc:** Who contain all information to build the image.
  Inside that **FOLDER** you have:
  - **packer_main.json** : Describe the steps to build the docker image used from packer tool.
  - **packer_main.yml** : Is the ansible playbook called in packer_main.json used to configure the image
  - **env_vars** :  who define all stuffs you need to build properly an image.\
   Basically in env_vars you need to specify:
    - image_src: the base image to use for build your image
    - image_dest: the name of your image at the end of the build
    - env_target: (dev,qa,etc) usually you need to use dev
    - packer_build: the build number of your image

<br><br>   
## Create a new build process 
 - Easy Way
   - duplicate an existing build folder es. java_jdk
   - Rename it base and the beaviour you want to have in your custom image
   - udpate env_vars  properly
   - create your custom ansible roles in ansible/roles folder
   - update packer_main.yml playbook calling your custom ansible roles.
   - build.sh your_custom_build_folder
  
<br><br>
 ## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change. 