# Aws stack with lamp server and salt-stack management using separed vpc and bastion host

The test-stack.json create
 
* 	1 public vpc 
* 	1 private vpc 
* 	1 nat setup and routing to permit host in private vpc to access on internet for update repository
* 	3 security group rules with specific port (frontend , backend, bastionend)
* 	1 create a  bastion host in public vpc  for manage host in private vpc without espose those host directly on internet
* 	1 setup to bastion with 	
	  - salt-stack management
	  - file conf to deploy apache/php server from salt-stack
	  - file conf to deploy mysql server from salt-stack

When the stack it's work,  a boot script in bastion host use salt-stack to create 

*       1 host in public vpc and configure nginx and php
* 	1 host in private vpc and configure mariadb 
* 	1 setup simple app php to read mysql table 

 Install aws cli or  upload file in aws management console web (cloudformation service)
Command in bash

	aws cloudformation create-stack --stack-name myteststack --template-body file://test-stack.json   --capabilities CAPABILITY_IAM

