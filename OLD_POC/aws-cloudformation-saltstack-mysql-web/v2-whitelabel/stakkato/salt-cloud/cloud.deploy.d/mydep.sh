add-apt-repository -y ppa:saltstack/salt
apt-get update && apt-get -y upgrade
apt-get -y install salt-api
apt-get -y install salt-minion
apt-get -y install salt-ssh
apt-get -y install salt-syndic
mv /etc/salt/minion /etc/salt/minion.backup
rm -f /etc/salt/minion_id
cat <<EOF >  /etc/salt/minion
master: $MASTER
EOF
systemctl stop salt-master.service
systemctl disable salt-master.service
/etc/init.d/salt-minion restart
