
web_srv:
  pkg:
    - installed
    - pkgs:
      - nginx
      - php7.0
      - php7.0-fpm
      - php7.0-cli
      - php7.0-gd
      - php7.0-mcrypt
      - php7.0-mysql

install-php-mcrypt-fpm:
  file.symlink:
    - name: /etc/php/7.0/fpm/conf.d/20-mcrypt.ini
    - target: /etc/php/7.0/mods-available/mcrypt.ini
    - require:
      - pkg: web_srv

install-php-mcrypt-cli:
  file.symlink:
    - name: /etc/php/7.0/cli/conf.d/20-mcrypt.ini
    - target: /etc/php/7.0/mods-available/mcrypt.ini
    - require:
      - pkg: web_srv
