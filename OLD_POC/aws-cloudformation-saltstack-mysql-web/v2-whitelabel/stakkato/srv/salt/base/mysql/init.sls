

debconf-utils:
  pkg: 
    - installed

python-mysqldb:
  pkg:
    - installed

python-pymysql:
  pkg:
    - installed


mysql_setup:
  debconf.set:
    - name: mysql-server
    - data:
        'mysql-server/root_password': {'type': 'password', 'value': '{{ salt['pillar.get']('mysql:root_pw', '') }}' }
        'mysql-server/root_password_again': {'type': 'password', 'value': '{{ salt['pillar.get']('mysql:root_pw', '') }}' }
    - require:
      - pkg: debconf-utils


mysql:
  pkg.installed:
    - name: mysql-server
  service:
    - running
    - enable: True
    - restart: True
    - watch:
      - file: /etc/mysql/mysql.conf.d/mysqld.cnf
    - require:
      - debconf: mysql-server
    

/etc/salt/minion.d/mysql.conf:
  file.managed:
    - source: salt://mysql/files/mysql-minion.conf
    - user: root
    - group: root
    - mode: 640
    - require:
      - pkg: mysql-server


/etc/mysql/salt.cnf:
  file.managed:
    - source: salt://mysql/files/salt.cnf.jinja
    - template: jinja
    - user: root
    - group: root
    - mode: 640
    - require:
      - pkg: mysql-server


/root/.my.cnf:
  file:
    - managed
    - source: salt://mysql/files/root-my.cnf
    - user: root
    - group: root
    - mode: 0600
    - template: jinja


/etc/mysql/mysql.conf.d/mysqld.cnf:
 file.managed:
    - source: salt://mysql/files/mysqld.cnf.jinja
    - template: jinja
    - user: root
    - group: root
    - mode: 640
    - require:
      - pkg: mysql-server


mysql-root-user-remote:
  mysql_user.present:
    - name: root
    - host: '%'
    - password: {{ salt['pillar.get']('mysql:root_pw', '') }}
    - connection_user: root
    - connection_pass: {{ salt['pillar.get']('mysql:root_pw', '') }} 
    - connection_charset: utf8
    - saltenv:
      - LC_ALL: "en_US.utf8"
    - require:
      - pkg: mysql-server

  mysql_grants.present:
    - grant: all privileges
    - database: "*.*"
    - user: root
    - host: '%'
    - connection_user: root
    - connection_pass: {{ salt['pillar.get']('mysql:root_pw', '') }}




test_us:
  mysql_user.present:
    - name: {{ salt['pillar.get']('test_us:user', '') }}
    - host: '%'
    - password: {{ salt['pillar.get']('test_us:passwd', '') }}
    - connection_user: root
    - connection_pass: {{ salt['pillar.get']('mysql:root_pw', '') }} 
    - connection_charset: utf8

  mysql_grants.present:
    - grant: all privileges
    - database: test.*
    - user: {{ salt['pillar.get']('test_us:user', '') }}
    - password: {{ salt['pillar.get']('test_us:passwd', '') }}
    - host: '%'
    - connection_user: root
    - connection_pass: {{ salt['pillar.get']('mysql:root_pw', '') }} 
    - connection_charset: utf8
    - require:
        - mysql_user: {{ salt['pillar.get']('test_us:user', '') }}
        - mysql_database: test

test:
  mysql_database.present:
  - name: test
  - connection_user: root
  - connection_pass: {{ salt['pillar.get']('mysql:root_pw', '') }}
  - connection_charset: utf8

/tmp/dump.db:
  file.managed:
    - source: salt://mysql/files/dump.db
    - user: root
    - group: root
    - mode: 640

restore_db:
  cmd.run:
      - name: "mysql -h {{ grains['ip4_interfaces'].eth0[0] }}  -D test -u  {{ salt['pillar.get']('test_us:user', '') }}  -p{{ salt['pillar.get']('test_us:passwd', '') }}  < /tmp/dump.db "
