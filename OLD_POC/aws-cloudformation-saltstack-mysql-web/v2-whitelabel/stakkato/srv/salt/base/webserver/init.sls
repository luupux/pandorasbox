
/home/www:
  file.directory:
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

/home/www/conf.php:
  file.managed:
    - source: salt://webserver/files/app/conf.php.jinja
    - template: jinja
    - user: root
    - group: root
    - mode: 644

/home/www/index.php:
  file.managed:
    - source: salt://webserver/files/app/index.php
    - user: root
    - group: root
    - mode: 644

include:
  - .nginx


