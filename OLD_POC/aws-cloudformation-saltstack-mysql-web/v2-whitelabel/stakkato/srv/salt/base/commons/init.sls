{% set the_node_type = salt['grains.get']('node_type', '') %}

commons:
  pkg.installed:
    - pkgs:
      - vim
      - mysql-client


/etc/vim/vimrc:
  file.append:
    - text: 
       - syntax on
       - set nocompatible
       - set tabstop=2
       - set shiftwidth=2
       - set expandtab
